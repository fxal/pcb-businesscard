# Printed circuit board business card
This is a landing page repository for people who received my pcb business card.

You can find here the bill of materials, schematics, board designs and firmware required for using the pcb business card.

Once assembled and flashed, this project features a Windows and Linux compatible keyboard surrogate with a total of three buttons: **Left Control**, **C** and **V** in lowercase -- all that a good software engineer would need:

![](images/top_view.jpg)

## Bill of materials
To complete this pcb business card to have it function as a keyboard, you will require the following hardware components:
- Arduino Pro Micro (or an Atmega32u4 compatible clone) x1
- Cherry MX switch (of your liking) x3
- Cherry MX switch compatible keys x3
- 5mm LED (THT) x4
- Resistor 160Ohm (0207 THT) x4
- Micro USB to USB A cable x1

## Soldering, assembly and flashing
1. Solder the resistors R1, R2, R3 and R4.
2. Solder the Arduino Pro Micro with the connector facing the right outer side. The solder mask indicates the location of the USB connector.
3. Turn the board around and solder the four LEDs. The LEDs shall act as stands for the board, so they must face the bottom side. Make sure the polarity matches the bottom solder mask.
4. Then solder the three Cherry MX buttons. 
5. Gently push the key caps on. 
6. Flash the micro controller with the source code provided in this project:
	1. If not already installed, download the Arduino IDE from the official web site.
	2. Download and open the .ino file in this project located under [firmware/pcb-businesscard/pcb-businesscard.ino](firmware/pcb-businesscard/pcb-businesscard.ino).
	3. Plug in the USB cable into the Arduino and connect it to your computer.
	4. Choose the correct port of the Arduino and the correct type: Arduino Leonardo. The port should auto detect. If not, choose it in the IDE under Tools -&gt; Port
	5. Press *Upload* and wait for the file to compile and flash to the Arduino.
7. You now have your own **CTRL-C** and **CTRL-V** keyboard with some light effects. Congratulations!

## Images
Front:

![](images/businesscard_front.png)

Back:

![](images/businesscard_back.png)

---

The LEDs have a fade effect:

![](images/fade.gif)

Populated and plugged in board:

![](images/top_pluggedin.jpg)

Back side of the pcb with resistors not yet soldered:

![](images/backside_resistors.jpg)

Back side with LEDs soldered:

![](images/backside_leds.jpg)

## References
Thanks to https://github.com/c0z3n/cherrymx-eagle for the Cherry MX library.

Also thanks to https://github.com/sparkfun/SparkFun-Eagle-Libraries for the Arduino Pro Micro library.