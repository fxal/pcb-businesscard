/**
 * CTRL+C CTRL+V business card
 *
 * See gitlab.com/fxal/pcb-businesscard for BOM.
 *
 * @author Felix Almer
 * @version 14.11.2020
 */

#include "Keyboard.h"

// An approximation of degree to radians and return calculation
#define DEGR_TO_RAD(D) (D * 1000.0f / 57296.0f)
#define RAD_TO_DEG(R) (R * 57296.0f / 1000.0f)

#define BTN_CTRL_OUT 16
#define BTN_CTRL_IN 10
#define BTN_C_OUT 15
#define BTN_C_IN 14
#define BTN_V_OUT A1
#define BTN_V_IN A0

#define LED_STATUS 17

#define LED_1 3
#define LED_2 5
#define LED_3 6
#define LED_4 9

#define LED_MIN_INTENSITY 10
#define LED_MAX_INTENSITY 255

const unsigned long debounceDelayMillis = 50L;
const unsigned long ledFadeStepDelayMillis = 50L;
volatile unsigned long lastLedFadeStepDelay = 0L;

volatile uint16_t phi = 0;
const uint16_t phiStep = 2;

/**
* A struct to represent an LED.
*
* @param delta the phase offset. i.e. the sinus curve offset of the intensity over time.
* @param pin the pin where the LED is located on the Arduino.
* @param intensity the current LED illumination pwm intensity driving the pin.
* @param minIntensity the minimum allowed illumination pwm intensity of this LED.
* @param maxIntensity the maximum allowed illumination pwm intensity of this LED.
*/
struct Led {
   uint16_t delta;
   int pin;
   uint8_t intensity;
   uint8_t minIntensity;
   uint8_t maxIntensity;
};

/**
* A struct to represent a physical button and its virtual character counterpart.
* By pressing the physical button, a virtual character counterpart is sent to the connected computer simulating a keyboard button press.
*
* @param state the current state of the physical button. i.e. pressed or released mapped to LOW and HIGH, respectively.
* @param inPin the Arduino pin where the physical button is polled for its state.
* @param outPin the Arduino pin prividing the expected reference voltage for the inPin.
* @param character the virtual keyboard character counterpart of the physical button: this is the character sent to the computer when the button is pressed.
* @param lastDebounce a timestamp when the last successful state change occurred. Used for debouncing the physical button.
*/
struct Button {
   int state;
   int inPin;
   int outPin;
   char character;
   unsigned long lastDebounce;
};

Led* leds[4] = { new Led(), new Led(), new Led(), new Led() };
Button* buttons[3] = { new Button(), new Button(), new Button() };

/**
* Setup. Gets all pins and variables ready for operation.
*/
void setup() {
   setButtonFields(buttons[0], KEY_LEFT_CTRL, HIGH, BTN_CTRL_IN, BTN_CTRL_OUT, 0L);
   setButtonFields(buttons[1], 'v', HIGH, BTN_V_IN, BTN_V_OUT, 0L);
   setButtonFields(buttons[2], 'c', HIGH, BTN_C_IN, BTN_C_OUT, 0L);

   for(uint8_t i = 0; i < 3; i++) {
       setupButton(buttons[i]);
   }

   setLedFields(leds[0], 0, LED_1, LED_MIN_INTENSITY, LED_MIN_INTENSITY, LED_MAX_INTENSITY);
   setLedFields(leds[1], 0, LED_2, LED_MIN_INTENSITY, LED_MIN_INTENSITY, LED_MAX_INTENSITY);
   setLedFields(leds[2], 0, LED_3, LED_MIN_INTENSITY, LED_MIN_INTENSITY, LED_MAX_INTENSITY);
   setLedFields(leds[3], 0, LED_4, LED_MIN_INTENSITY, LED_MIN_INTENSITY, LED_MAX_INTENSITY);

   for(uint8_t i = 0; i < 4; i++) {
       setupLed(leds[i]);
   }

   Keyboard.begin();
}

/**
* Loop. Recurring Arduino routine.
*/
void loop() {
   for(uint8_t i = 0; i < 3; i++) {
      debouncedReading(buttons[i]);
   }

   fadeLeds();
}

/**
* SetLedFields. Populates a given Led struct reference with the provided values.
*
* @param led the Led struct reference to be populated.
* @param delta the phase offset of this LED.
* @param pin the pin where the LED is driven from.
* @param intensity the initial intensity of this LED.
* @param minIntensity the minimum allowed pwm intensity.
* @param maxIntensity the maximum allowed pwm intensity.
*/
void setLedFields(Led* led, uint16_t delta, int pin, uint8_t intensity, uint8_t minIntensity, uint8_t maxIntensity) {
   led->delta = delta;
   led->pin = pin;
   led->intensity = intensity;
   led->minIntensity = minIntensity;
   led->maxIntensity = maxIntensity;
}

/**
* SetupLed. Sets the pin mode of the LED pin and initializes it with the provided intensity.
*
* @param led the led that shall be set up.
*/
void setupLed(Led* led) {
   pinMode(led->pin, OUTPUT);
   analogWrite(led->pin, led->intensity);
}

/**
* FadeLeds. Sets the intensity of all LEDs by one step for each time this function is called.
* The fading follows the sinus function.
*
* Side effect: The phi variable is incremented by phiStep modulo 360.
*/
void fadeLeds() {
   unsigned long now = millis();
   if((now - lastLedFadeStepDelay) > ledFadeStepDelayMillis) {
      lastLedFadeStepDelay = now;
      for(uint8_t i = 0; i < 4; i++) {
         leds[i]->intensity = leds[i]->minIntensity 
		    + abs(sin(DEGR_TO_RAD(leds[i]->delta + phi))) * (leds[i]->maxIntensity - leds[i]->minIntensity);
         analogWrite(leds[i]->pin, leds[i]->intensity);
      }

      phi = (phi += phiStep) % 360;
   }
}

/**
* SetButtonFields. Populates a given Button struct reference with the provided parameters.
*
* @param button the button struct reference to be populated.
* @param character the virtual character this button is assigned to.
* @param initialState the initial state (pressed or released / LOW or HIGH) of this button.
* @param inPin the Arduino pin where the physical button is polled for its state.
* @param outPin the Arduino pin prividing the expected reference voltage for the inPin.
* @param lastDebounce a timestamp when the last successful state change occurred.
*/
void setButtonFields(Button* button, char character, int initialState, int inPin, int outPin, unsigned long lastDebounce) {
   button->state = initialState;
   button->character = character;
   button->inPin = inPin;
   button->outPin = outPin;
   button->lastDebounce = lastDebounce;
}

/**
* SetupButton. Sets the pins of the provided button and initializes them with the expected values.
*
* @param button the button that shall be set up.
*/
void setupButton(Button* button) {
   pinMode(button->inPin, INPUT_PULLUP);
   pinMode(button->outPin, OUTPUT);
   digitalWrite(button->outPin, LOW);
}

/**
* DebouncedReading. Reads the inPin of the provided button, does debouncing according to the buttons last state change and, if applicable, calls
* the related function for sending virtual key strokes to the connected computer.
*
* @param button the button that is checked for state change.
*/
void debouncedReading(Button* button) {
   int reading = digitalRead(button->inPin);
   if(reading != button->state) {
      unsigned long now = millis();
      if((now - button->lastDebounce) > debounceDelayMillis) {
         button->state = reading;
         pressOrReleaseButton(button);
         button->lastDebounce = now;
       }
   }
}

/**
* PressOrReleaseButton. Presses or releases the virtual keyboard button depending on the new state value.
* Also inverts the notification LED.
*
* @param button the button holding the character to be sent to the computer as pressed or released depending on its state.
*/
void pressOrReleaseButton(Button* button) {
   if(button->state == LOW) {
       Keyboard.press(button->character);
   } else {
       Keyboard.release(button->character);
   }
   digitalWrite(LED_STATUS, !digitalRead(LED_STATUS));
}
